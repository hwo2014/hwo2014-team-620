(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg :msgType)

(defmethod handle-msg "yourCar" [msg state]
  {:to-server nil
   :game-id (:gameId msg)
   :car (:data msg)})

(defmethod handle-msg "gameInit" [msg state]
  (merge {:to-server nil}
         (-> msg :data :race)))

(defn get-speed [position-info]
  (- 1.0 (/ (:angle position-info) 90.0)))

(defn find-car-position [my-car car-positions]
  (first (filter #(= (:id %) my-car) car-positions)))

(defmethod handle-msg "carPositions" [msg state]
  (let [position (find-car-position (state :car) (:data msg))
        speed (get-speed position)]
    {:to-server {:msgType "throttle" :data speed}}))

(defmethod handle-msg "join" [msg state]
  {})

(defmethod handle-msg :default [msg state]
  {})

(defmethod handle-msg "gameStart" [msg state]
  {})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "yourCar" (println "Got yourCar")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel prev-race-state]
  (let [msg (read-message channel)
        race-state (merge prev-race-state (handle-msg msg prev-race-state))]
    (println "<<<<<")
    (clojure.pprint/pprint msg)
    (when (:to-server race-state)
      (send-message channel (:to-server race-state)))
    (println ">>>>>")
    (clojure.pprint/pprint race-state)
    (recur channel race-state)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel {})))
